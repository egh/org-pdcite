(require 'f)

(defvar org-pdcite-root-path
  (f-parent (f-dirname load-file-name)))

(add-to-list 'load-path org-pdcite-root-path)

(require 'org)
(require 'org-pdcite)
